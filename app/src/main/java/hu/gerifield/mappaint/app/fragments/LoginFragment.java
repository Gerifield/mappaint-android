package hu.gerifield.mappaint.app.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import hu.gerifield.mappaint.app.MainMapActivity;
import hu.gerifield.mappaint.app.MapPaintApp;
import hu.gerifield.mappaint.app.R;
import hu.gerifield.mappaint.app.model.LoginData;
import hu.gerifield.mappaint.app.model.User;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by gerifield on 2014.04.06..
 */
public class LoginFragment extends Fragment implements View.OnClickListener {

    private Button loginButton;
    private EditText userEdit, passEdit;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){

        View view = inflater.inflate(R.layout.login_fragment_layout, container, false);

        userEdit = (EditText) view.findViewById(R.id.login_userEditText);
        passEdit = (EditText) view.findViewById(R.id.login_passwordEditText);

        loginButton = (Button) view.findViewById(R.id.login_loginButton);
        loginButton.setOnClickListener(this);


        return view;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.login_loginButton:
                Log.d("LOGIN_LOG", "Do the login stuff button...");
                //ProgressDialog.show(getActivity(),"Title", "Message");
                LoginData loginData = new LoginData();
                loginData.user = userEdit.getText().toString();
                loginData.password = passEdit.getText().toString();

                ((MapPaintApp)getActivity().getApplicationContext()).getMapPaintService().login(loginData, new Callback<User>() {
                    @Override
                    public void success(User user, Response response) {
                        Log.d("LOGIN_FRAGMENT", response.toString());
                        SharedPreferences pref = getActivity().getSharedPreferences("AuthData", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = pref.edit();
                        editor.putInt("Uid", user.data.getId());
                        editor.putString("Authtoken", user.data.getToken());
                        editor.commit();

                        Intent i = new Intent(getActivity(), MainMapActivity.class);
                        startActivity(i);
                        getActivity().finish();
                    }

                    @Override
                    public void failure(RetrofitError retrofitError) {
                        Log.d("LOGIN_FRAGMENT", retrofitError.toString());
                        Toast.makeText(getActivity().getApplicationContext(), getString(R.string.wrong_user_or_pass_toast), Toast.LENGTH_SHORT).show();
                    }
                });
                break;
        }
    }
}
