package hu.gerifield.mappaint.app.comm;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import retrofit.RequestInterceptor;

/**
 * Created by gerifield on 2014.04.19..
 */
public class MyInterceptor implements RequestInterceptor {

    private SharedPreferences sharedPreferences;

    public MyInterceptor(SharedPreferences sharedPreferences){
        this.sharedPreferences = sharedPreferences;
    }

    @Override
    public void intercept(RequestFacade requestFacade) {
        //requestFacade.addHeader();
        requestFacade.addHeader("Uid", String.valueOf(sharedPreferences.getInt("Uid", 0)));
        requestFacade.addHeader("Authtoken", sharedPreferences.getString("Authtoken", ""));
    }
}
