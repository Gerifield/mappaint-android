package hu.gerifield.mappaint.app.fragments;

        import android.app.ProgressDialog;
        import android.content.Context;
        import android.content.Intent;
        import android.content.SharedPreferences;
        import android.os.Bundle;
        import android.support.v4.app.Fragment;
        import android.support.v4.app.FragmentActivity;
        import android.util.Log;
        import android.view.LayoutInflater;
        import android.view.View;
        import android.view.ViewGroup;
        import android.widget.Button;
        import android.widget.EditText;
        import android.widget.Toast;

        import hu.gerifield.mappaint.app.MainMapActivity;
        import hu.gerifield.mappaint.app.MapPaintApp;
        import hu.gerifield.mappaint.app.R;
        import hu.gerifield.mappaint.app.model.RegData;
        import hu.gerifield.mappaint.app.model.User;
        import retrofit.Callback;
        import retrofit.RetrofitError;
        import retrofit.client.Response;

/**
 * Created by gerifield on 2014.04.06..
 */
public class RegistrationFragment extends Fragment implements View.OnClickListener {

    private Button regButton;
    private EditText userEdit, emailEdit, passEdit, passEdit2;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){

        View view = inflater.inflate(R.layout.registration_fragment_layout, container, false);

        userEdit = (EditText) view.findViewById(R.id.reg_userEditText);
        emailEdit = (EditText) view.findViewById(R.id.reg_emailEditText);
        passEdit = (EditText) view.findViewById(R.id.reg_passwordEditText);
        passEdit2 = (EditText) view.findViewById(R.id.reg_passwordEditText2);

        regButton = (Button) view.findViewById(R.id.reg_regButton);
        regButton.setOnClickListener(this);

        return view;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.reg_regButton:
                Log.d("REG_LOG", "Do the reg stuff");
                //ProgressDialog.show(getActivity(), "Title", "Message");
                RegData rd = new RegData();
                rd.user = userEdit.getText().toString();
                rd.email = emailEdit.getText().toString();
                rd.password = passEdit.getText().toString();
                rd.password2 = passEdit2.getText().toString();

                ((MapPaintApp) getActivity().getApplicationContext()).getMapPaintService().register(rd, new Callback<User>() {
                    @Override
                    public void success(User user, Response response) {
                        Log.d("REG_FRAGMENT", response.toString());
                        SharedPreferences pref = getActivity().getSharedPreferences("AuthData", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = pref.edit();
                        editor.putInt("Uid", user.data.getId());
                        editor.putString("Authtoken", user.data.getToken());
                        editor.commit();

                        Intent i = new Intent(getActivity(), MainMapActivity.class);
                        startActivity(i);
                        getActivity().finish();
                    }

                    @Override
                    public void failure(RetrofitError retrofitError) {
                        Log.d("REG_FRAGMENT", retrofitError.toString());
                        Toast.makeText(getActivity().getApplicationContext(), getString(R.string.wrong_user_or_pass_toast), Toast.LENGTH_SHORT).show();
                    }
                });

                break;
        }
    }
}
