package hu.gerifield.mappaint.app.model;

import java.util.List;

/**
 * Created by gerifield on 2014.04.11..
 */
public class Markers extends DefaultModel{

    public List<MarkerData> data;

    public List<MarkerData> getData() {
        return data;
    }

    public void setData(List<MarkerData> data) {
        this.data = data;
    }

    public static class MarkerData {
        public int id;
        public int user_id;
        public double lat;
        public double lon;
        public String creation_date;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getUser_id() {
            return user_id;
        }

        public void setUser_id(int user_id) {
            this.user_id = user_id;
        }

        public double getLat() {
            return lat;
        }

        public void setLat(double lat) {
            this.lat = lat;
        }

        public double getLon() {
            return lon;
        }

        public void setLon(double lon) {
            this.lon = lon;
        }

        public String getCreation_date() {
            return creation_date;
        }

        public void setCreation_date(String creation_date) {
            this.creation_date = creation_date;
        }
    }
}
