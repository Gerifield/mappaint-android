package hu.gerifield.mappaint.app.model;

/**
 * Created by gerifield on 2014.05.04..
 */
public class SingleMarker extends DefaultModel {
    public Markers.MarkerData data;

    public Markers.MarkerData getData() {
        return data;
    }

    public void setData(Markers.MarkerData data) {
        this.data = data;
    }
}
