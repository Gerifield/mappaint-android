package hu.gerifield.mappaint.app.model;

import java.util.List;

/**
 * Created by gerifield on 2014.05.04..
 */
public class Connections extends DefaultModel{

    public List<ConnectionPair> data;

    public List<ConnectionPair> getData() {
        return data;
    }

    public void setData(List<ConnectionPair> data) {
        this.data = data;
    }

    public static class ConnectionPair{
        public Markers.MarkerData markera;
        public Markers.MarkerData markerb;

        public Markers.MarkerData getMarkera() {
            return markera;
        }

        public void setMarkera(Markers.MarkerData markera) {
            this.markera = markera;
        }

        public Markers.MarkerData getMarkerb() {
            return markerb;
        }

        public void setMarkerb(Markers.MarkerData markerb) {
            this.markerb = markerb;
        }
    }
}
