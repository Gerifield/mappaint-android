package hu.gerifield.mappaint.app;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.HashMap;

import hu.gerifield.mappaint.app.model.Connections;
import hu.gerifield.mappaint.app.model.DefaultModel;
import hu.gerifield.mappaint.app.model.Markers;
import hu.gerifield.mappaint.app.model.SingleMarker;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class MainMapActivity extends FragmentActivity implements GoogleMap.OnMapClickListener, AdapterView.OnItemClickListener, GoogleMap.OnMarkerClickListener {

    private GoogleMap gmap;
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private TextView modeTextView;
    private MapPaintApp thisapp;
    private ActionBarDrawerToggle mActionBarDrawerToggle;
    private HashMap<String, Markers.MarkerData> markerDataStore;
    private Marker lastMarker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        mDrawerList = (ListView) findViewById(R.id.left_drawer);
        mDrawerList.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, getResources().getStringArray(R.array.drawer_layout_items)));
        mDrawerList.setOnItemClickListener(this);

        mActionBarDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
                                    R.drawable.ic_drawer,
                                    R.string.drawer_open,
                                    R.string.drawer_close){

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                getActionBar().setTitle(R.string.app_name);
            }

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                getActionBar().setTitle(R.string.menu_name);
            }
        };
        mDrawerLayout.setDrawerListener(mActionBarDrawerToggle);
        getActionBar().setDisplayHomeAsUpEnabled(true);

//        getActionBar().setHomeButtonEnabled(true); //API 14...

        markerDataStore = new HashMap<String, Markers.MarkerData>();

        thisapp = (MapPaintApp) getApplication();
        modeTextView = (TextView) findViewById(R.id.main_mode_textView);
        modeTextView.setText(getResources().getStringArray(R.array.drawer_layout_mode_selected_items)[thisapp.getMarkerMode()]);


        gmap = ((SupportMapFragment)getSupportFragmentManager().findFragmentById(R.id.googlemap)).getMap();

        if(gmap == null){
            Toast.makeText(getApplicationContext(), getString(R.string.map_fatal_error), Toast.LENGTH_SHORT).show();
            finish();
        }else{
            gmap.setMyLocationEnabled(true);
            //gmap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
            //gmap.setTrafficEnabled(true);

//            gmap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(47.4072002,18.9485949)));
//            gmap.animateCamera(CameraUpdateFactory.zoomTo(17));
            CameraPosition campos = new CameraPosition.Builder()
                    .target(new LatLng(47.4076682,18.9509798))
                    .zoom(17)
                    //.bearing(90)
                    .build();
            gmap.animateCamera(CameraUpdateFactory.newCameraPosition(campos));
            gmap.setOnMapClickListener(this);
            gmap.setOnMarkerClickListener(this);

//            gmap.addMarker(new MarkerOptions()
//                    .position(new LatLng(0, 0))
//                    .title("Hello!"));


            //fetch markers and connections
            updateMapData();

        }

    }

    public void updateMapData(){
        gmap.clear(); //clear the map

        //Fetch markers
        thisapp.getMapPaintService().getMarkers(new Callback<Markers>() {
            @Override
            public void success(Markers markers, Response response) {
                Log.d("RETROFIT", markers.toString());
                for(Markers.MarkerData md : markers.data){

                    Marker m = gmap.addMarker(new MarkerOptions()
                            .position(
                                    new LatLng(md.lat,md.lon)
                            ));
                    markerDataStore.put(m.getId(), md);
                    Log.d("Fetch MARKER", md.toString());
                }
            }

            @Override
            public void failure(RetrofitError retrofitError) {
                Log.d("RETROFIT", retrofitError.toString());
                Toast.makeText(getApplication(), getString(R.string.network_error), Toast.LENGTH_SHORT).show();
            }
        });

        thisapp.getMapPaintService().getConnections(new Callback<Connections>() {
            @Override
            public void success(Connections connections, Response response) {
                Log.d("RETROFIT", connections.toString());

                for(Connections.ConnectionPair cp : connections.data){
                    PolylineOptions lineopts = new PolylineOptions()
                            .add(new LatLng(cp.markera.lat, cp.markera.lon))
                            .add(new LatLng(cp.markerb.lat, cp.markerb.lon));
                    //Polyline lastLine = gmap.addPolyline(lineopts);
                    gmap.addPolyline(lineopts); //I don't need to store these
//                    Log.d("RETROFIT", "M1: "+cp.markera.id+", M2: "+cp.markerb.id);
                }
            }

            @Override
            public void failure(RetrofitError retrofitError) {
                Log.d("RETROFIT", retrofitError.toString());
                Toast.makeText(getApplication(), getString(R.string.network_error), Toast.LENGTH_SHORT).show();
            }
        });
    }


    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mActionBarDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mActionBarDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Pass the event to ActionBarDrawerToggle, if it returns
        // true, then it has handled the app icon touch event
        if (mActionBarDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        // Handle your other action bar items...

        return super.onOptionsItemSelected(item);
    }




    @Override
    public void onStop(){
        super.onStop();

        thisapp = null; //remove binding to avoid leak?
    }

    @Override
    public void onMapClick(LatLng latLng) { //add marker
        Log.d("MAPCLICK", latLng.toString());

        switch (thisapp.getMarkerMode()){
            case 0:
                //add marker!

                final Marker m = gmap.addMarker(new MarkerOptions()
                        .position(latLng)); //??????????????????????????

                final Markers.MarkerData md = new Markers.MarkerData();
                md.setLat(latLng.latitude);
                md.setLon(latLng.longitude);


                thisapp.getMapPaintService().addMarker(md, new Callback<SingleMarker>() {
                    @Override
                    public void success(SingleMarker singleMarker, Response response) {
                        //OK
                        Log.d("ADD MARKER REST", response.toString());
                        Toast.makeText(getApplicationContext(), getString(R.string.marker_added), Toast.LENGTH_SHORT).show();
                        markerDataStore.put(m.getId(), singleMarker.data); //add inside success?
                    }

                    @Override
                    public void failure(RetrofitError retrofitError) {
                        //OOPS
                        Log.d("ADD MARKER REST", retrofitError.toString());
                        m.remove();
                        Toast.makeText(getApplicationContext(), getString(R.string.network_error), Toast.LENGTH_SHORT).show();
                    }
                });
                break;
//            case 1:
//                //connect markers
//                break;
//            case 2:
//                //delete markers
//                break;
        }
    }


    @Override
    public boolean onMarkerClick(final Marker marker) { //connect or delete

        if(thisapp.getMarkerMode() == 2){
            //delete
            Markers.MarkerData mdata = markerDataStore.get(marker.getId());
            Log.d("CLICK MARKER", marker.toString());
            Log.d("CLICK MARKER", mdata.toString());

            thisapp.getMapPaintService().delMarker(mdata.getId(), new Callback<DefaultModel>() {
                @Override
                public void success(DefaultModel defaultModel, Response response) {
                    Log.d("MARKER DELETE", response.toString());
                    Toast.makeText(getApplicationContext(), getString(R.string.marker_deleted), Toast.LENGTH_SHORT).show();
                    //marker.remove();
                    updateMapData(); //update lines and stuff
                }

                @Override
                public void failure(RetrofitError retrofitError) {
                    Log.d("MARKER DELETE", retrofitError.toString());
                    Toast.makeText(getApplicationContext(), getString(R.string.network_error), Toast.LENGTH_SHORT).show();
                }
            });
            return true; //consumed

        }else if(thisapp.getMarkerMode() == 1){
            //connect
            if(lastMarker != null){
                //connect them!
                PolylineOptions lineopts = new PolylineOptions()
                        .add(lastMarker.getPosition())
                        .add(marker.getPosition());
                final Polyline lastLine = gmap.addPolyline(lineopts);

                Markers.MarkerData lastmdata = markerDataStore.get(lastMarker.getId());
                Markers.MarkerData currmdata = markerDataStore.get(marker.getId());

                thisapp.getMapPaintService().connectMarker(lastmdata.getId(), currmdata.getId(), new Callback<DefaultModel>() {
                    @Override
                    public void success(DefaultModel defaultModel, Response response) {
                        Log.d("CONNECT MARKER", response.toString());
                        Toast.makeText(getApplicationContext(), getString(R.string.markers_connected), Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void failure(RetrofitError retrofitError) {
                        Log.d("CONNECT MARKER", retrofitError.toString());
                        Toast.makeText(getApplicationContext(), getString(R.string.network_error), Toast.LENGTH_SHORT).show();
                        lastLine.remove();
                    }
                });

            }

            lastMarker = marker; //store the last

            return true;
        }

        return false;
    }


    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        //Log.d("MENUCLICK", adapterView.toString());
        //Log.d("MENUCLICK", "I: "+i+", L: "+l);

        if (thisapp == null){ //hmm null check
            return;
        }
        lastMarker = null;

        switch (i){
            case 0:
                //add marker (mode: 0, default)
                Log.d("MENUCLICK", "Add marker");
                thisapp.setMarkerMode(0);

                modeTextView.setText(getResources().getStringArray(R.array.drawer_layout_mode_selected_items)[0]);
                mDrawerLayout.closeDrawers();
                break;
            case 1:
                //connect markers (mode: 1)
                Log.d("MENUCLICK", "Connect markers");
                thisapp.setMarkerMode(1);
                modeTextView.setText(getResources().getStringArray(R.array.drawer_layout_mode_selected_items)[1]);
                mDrawerLayout.closeDrawers();
                break;
            case 2:
                //delete marker (mode: 2)
                Log.d("MENUCLICK", "Delete marker");
                thisapp.setMarkerMode(2);
                modeTextView.setText(getResources().getStringArray(R.array.drawer_layout_mode_selected_items)[2]);
                mDrawerLayout.closeDrawers();
                break;
//          case 3:
//                //View v = getWindow().getDecorView().findViewById(android.R.id.content);
//                View v = getWindow().getDecorView().findViewById(R.id.main_drawer_screen_relative_layout);
//                //v.setDrawingCacheEnabled(true); //maybe store the old state and set back
//                v.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
//                        View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
//                v.layout(0, 0, v.getMeasuredWidth(), v.getMeasuredHeight());
//                v.buildDrawingCache(true);
//                Bitmap bmp = Bitmap.createBitmap(view.getDrawingCache());
//                v.setDrawingCacheEnabled(false);
//
//                FileOutputStream out;
//                try {
//
//                    File file = new File(Environment.getExternalStorageDirectory().getAbsoluteFile() + File.separator + "MapPaint_"+ new SimpleDateFormat("yyyy-MM-dd k:m:s").format(new Date())+".png");
//                    out = new FileOutputStream(file);
//                    bmp.compress(Bitmap.CompressFormat.PNG,90, out);
//                    out.flush();
//                    out.close();
//                }catch (Exception e){
//                    Log.d("SCREESHOT", e.toString());
//                }
//                break;
            case 3:
                //logout
                Log.d("MENUCLICK", "Logout");

                SharedPreferences pref = thisapp.getSharedPreferences("AuthData", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = pref.edit();
                editor.putInt("Uid", 0);
                editor.putString("Authtoken", "");
                editor.commit();

                Intent intent = new Intent(this, LoginActivity.class);
                startActivity(intent);
                finish();
                break;
        }
    }

}
