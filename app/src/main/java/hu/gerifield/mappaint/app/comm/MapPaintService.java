package hu.gerifield.mappaint.app.comm;

import com.google.android.gms.maps.model.Marker;

import java.util.List;

import hu.gerifield.mappaint.app.model.Connections;
import hu.gerifield.mappaint.app.model.DefaultModel;
import hu.gerifield.mappaint.app.model.LoginData;
import hu.gerifield.mappaint.app.model.Markers;
import hu.gerifield.mappaint.app.model.RegData;
import hu.gerifield.mappaint.app.model.SingleMarker;
import hu.gerifield.mappaint.app.model.User;
import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;

/**
 * Created by gerifield on 2014.04.11..
 */
public interface MapPaintService {

    //@GET("/user/{id}")
    //User getUser(@Path("id") int uid);

    @POST("/login")
    void login(@Body LoginData loginDataData, Callback<User> cb);

    @POST("/register")
    void register(@Body RegData regData, Callback<User> cb);

    @POST("/logout")
    void logout(Callback<DefaultModel> cb);

    @GET("/markers")
    void getMarkers(Callback<Markers> cb);

    @GET("/connections")
    void getConnections(Callback<Connections> cd);

    @POST("/marker")
    void addMarker(@Body Markers.MarkerData markerData, Callback<SingleMarker> cb);

    @DELETE("/marker/{id}")
    void delMarker(@Path("id") int mid, Callback<DefaultModel> cb);

    @POST("/connect/{id1}/{id2}")
    void connectMarker(@Path("id1") int mid1, @Path("id2") int mid2, Callback<DefaultModel> cb);
}
