package hu.gerifield.mappaint.app;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import hu.gerifield.mappaint.app.fragments.LoginFragment;
import hu.gerifield.mappaint.app.fragments.RegistrationFragment;

/**
 * Created by gerifield on 2014.04.19..
 */
public class LoginActivity extends FragmentActivity {

    Button registrationButton;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity_layout);


        if(findViewById(R.id.login_layout_fragment_holder) != null){ //fragment holder exists -> button too

            registrationButton = (Button) findViewById(R.id.reg_layout_button);
            registrationButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.d("LOGIN_LOG", "Reg fragment switch");
                    getSupportFragmentManager().beginTransaction()
                            //.setCustomAnimations(android.R.anim.slide_in_left, 0, 0, android.R.anim.slide_out_right)
                            //.setCustomAnimations(android.R.anim.fade_in, 0, 0, android.R.anim.fade_out)
                            .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out, android.R.anim.fade_in, android.R.anim.fade_out)
                            .replace(R.id.login_layout_fragment_holder, new RegistrationFragment(), "REGFRAGMENT")
                            .addToBackStack(null)
                            .commit();

                    registrationButton.setVisibility(View.GONE);
                }
            });

            //if(savedInstanceState == null) {
                //just login screen
                getSupportFragmentManager().beginTransaction()
                        .add(R.id.login_layout_fragment_holder, new LoginFragment(), "LOGINFRAGMENT")
                        .commit();
            //}else{
                if(getSupportFragmentManager().findFragmentByTag("REGFRAGMENT") != null){
                    //has reg fragment -> disable reg button!
                    registrationButton.setVisibility(View.GONE);
                }
            //}

        }else{
            //landscape mode
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        //hack....
        if(registrationButton != null)
            registrationButton.setVisibility(View.VISIBLE);
    }

}
