package hu.gerifield.mappaint.app;

import android.app.Application;

import com.squareup.otto.Bus;

import hu.gerifield.mappaint.app.comm.MapPaintService;
import hu.gerifield.mappaint.app.comm.MyInterceptor;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;

/**
 * Created by gerifield on 2014.04.11..
 */
public class MapPaintApp extends Application {

    //private static final String SERVER_URL = "http://127.0.0.1/";
    //private static final String SERVER_URL = "http://192.168.0.11/mappaint/";
    //private static final String SERVER_URL = "http://192.168.42.133/mappaint/";
    //private static final String SERVER_URL = "http://192.168.2.177/mappaint/";
    private static final String SERVER_URL = "http://mappaint.gerifield.hu";


    public int getMarkerMode() {
        return markerMode;
    }

    public void setMarkerMode(int markerMode) {
        this.markerMode = markerMode;
    }

    private int markerMode;

    private MapPaintService mapPaintService;
//    private Bus mBus;

    @Override
    public void onCreate(){
        super.onCreate();

        markerMode = 0;

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(SERVER_URL)
                .setRequestInterceptor(new MyInterceptor(getSharedPreferences("AuthData", MODE_PRIVATE)))
                .build();

        mapPaintService = restAdapter.create(MapPaintService.class);
    }

    public MapPaintService getMapPaintService(){
        return mapPaintService;
    }


//    public Bus getBus(){
//        if(mBus == null){
//            mBus = new Bus();
//        }
//        return mBus;
//    }
}
