package hu.gerifield.mappaint.app.model;

/**
 * Created by gerifield on 2014.04.24..
 */
public class DefaultModel {
    public int code;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}
