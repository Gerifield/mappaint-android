package hu.gerifield.mappaint.app.model;

/**
 * Created by gerifield on 2014.04.11..
 */
public class LoginData{
    public String user;
    public String password;

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
