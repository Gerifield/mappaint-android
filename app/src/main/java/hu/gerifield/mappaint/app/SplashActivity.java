package hu.gerifield.mappaint.app;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.maps.model.Marker;

import java.util.List;

import hu.gerifield.mappaint.app.fragments.LoginFragment;
import hu.gerifield.mappaint.app.model.Markers;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by gerifield on 2014.04.06..
 */
public class SplashActivity extends Activity {

    private static int SPLASH_TIME_OUT = 2000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_layout);


        //Check login here, don't start to download!
        ((MapPaintApp)getApplicationContext()).getMapPaintService().getMarkers(new Callback<Markers>() {
            @Override
            public void success(Markers markers, Response response) {
                Intent i = new Intent(SplashActivity.this, MainMapActivity.class);
                startActivity(i);
                finish();

            }

            @Override
            public void failure(RetrofitError retrofitError) {
                //relocate to login page on 401
                if(retrofitError.isNetworkError()) {
                    Toast.makeText(getApplicationContext(), getString(R.string.network_error), Toast.LENGTH_SHORT).show();
                    finish();
                }else{
                    if(retrofitError.getResponse().getStatus() == 401){
                        Intent i = new Intent(SplashActivity.this, LoginActivity.class);
                        startActivity(i);
                        finish();
                    }else{
                        Toast.makeText(getApplicationContext(), getString(R.string.fatal_error), Toast.LENGTH_SHORT).show();
                        finish();
                    }
                }
            }
        });

//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//
//                ((MapPaintApp)getApplication()).getMapPaintService().getMarkers(new Callback<List<Markers>>() {
//                    @Override
//                    public void success(List<Markers> markers, Response response) {
//                        Log.d("RETROFIT", markers.toString());
//                        Intent i = new Intent(SplashActivity.this, MainMapActivity.class);
//                        startActivity(i);
//                    }
//
//                    @Override
//                    public void failure(RetrofitError retrofitError) {
//                        Log.d("RETROFIT", retrofitError.toString());
//                        Toast.makeText(getApplication(), getString(R.string.network_error), Toast.LENGTH_LONG).show();
//                    }
//                });
//
//                //Intent i = new Intent(SplashActivity.this, MainMapActivity.class);
//                //startActivity(i);
//
//                finish();
//            }
//        }, SPLASH_TIME_OUT);

    }

}
